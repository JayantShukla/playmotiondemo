using UnityEngine;

public class Character:MonoBehaviour
{
    [SerializeField]
    private string m_CharacterName;
    [SerializeField]
    private HealthStat m_Health;
    [SerializeField]
    private DamageStat m_Damage;
    [SerializeField]
    private uint m_BuyPrice;
    [SerializeField]
    private uint m_BuildPrice;
    [SerializeField]
    private float m_BuildTime;

    public uint BuyPrice
    {
        get
        {
            return m_BuyPrice;
        }
    }

    public uint BuildPrice
    {
        get
        {
            return m_BuildPrice;
        }
    }

    public uint DamageValue
    {
        get
        {
            return m_Damage.CurrentValue;
        }
    }

    public uint HealthValue
    {
        get
        {
            return m_Health.CurrentValue;
        }
    }
    
    public void UpgradeStat(CharacterStat stat,User owner)
    {
        stat.UpgradeTier(owner);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User:MonoBehaviour
{
	private uint m_Currency = 0;
	private List<Character> m_CharactersOwned = null;

	public uint Currency
	{
		get
		{
			return m_Currency;
		}
		set
		{
			m_Currency = value;
		}
	}
	    
	void Awake()
	{
		DontDestroyOnLoad(gameObject);
		m_CharactersOwned = new List<Character>();
	}
	public void BuyCharacter(Character charToBuy)
	{
		if(charToBuy.BuyPrice <= Currency)
		{
			AddToOwnedCharacter(charToBuy);
			Currency -= charToBuy.BuyPrice;
		}
		else
		{
			Debug.LogError("Not enough money");
		}
	}

	public void BuildCharacter(Character charToBuild)
	{
		if(charToBuild.BuildPrice <= Currency)
		{
			AddCharacterToBuild(charToBuild);
			Currency -= charToBuild.BuildPrice;
		}
		else
		{
			Debug.LogError("Not enough money");
		}
	}

	public void AddCharacterToBuild(Character charToBuild)
	{
		//Wait for the Character BuildTime.
		//Then
		AddToOwnedCharacter(charToBuild);
	}

	public void AddToOwnedCharacter(Character OwnedCharacter)
	{
		m_CharactersOwned.Add(OwnedCharacter);
	}


}

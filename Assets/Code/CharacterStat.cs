using UnityEngine;
public abstract class CharacterStat:MonoBehaviour
{
    [SerializeField]
    private string m_StatName;
    [SerializeField]
    private uint m_BaseValue;
    [SerializeField]
    private uint[] m_TierPrices;
    [SerializeField]
    private uint[] m_TierValue;

    private int m_CurrentTier = -1;

    private uint m_CurrentValue;

    
    public uint CurrentValue
    {
        get
        {
           return m_CurrentValue;
            
        }
        set
        {
            m_CurrentValue = value;
        }
       
    }

    public uint BaseValue
    {
        get
        {
           return m_BaseValue;
            
        }
       
    }
    public int CurrentTier
    {
        get
        {
           return m_CurrentTier;
            
        }
        set
        {
            m_CurrentTier = value;
        }
    }
    public int MaxTierSets
    {
        get
        {
            if(m_TierPrices != null)
            {
                return m_TierPrices.Length;
            }
            else
            {
                return 0;
            }
            
        }
    }

    public uint TierPrice(int tier)
    {
        if(m_TierPrices != null && m_TierPrices.Length >= tier)
        {
            return m_TierPrices[tier];
        }
        else
        {
            return 0;
        }
    }
     public uint TierValue(int tier)
    {
        if(m_TierValue != null && m_TierValue.Length >= tier)
        {
            return m_TierValue[tier];
        }
        else
        {
            return 0;
        }
    }

    public abstract void OnUpgradeTier();
    public abstract void UpgradeTier(User owner);

}
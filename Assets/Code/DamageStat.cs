using UnityEngine;

public class DamageStat:CharacterStat
{
    void Start()
    {
        CurrentValue = BaseValue;
    }
    public override void OnUpgradeTier()
    {
        CurrentTier++;
        CurrentValue = TierValue(CurrentTier);
    }

    public override void UpgradeTier(User owner)
    {
        if(owner.Currency >= TierPrice(CurrentTier + 1))
        {
            owner.Currency -= TierPrice(CurrentTier + 1);
            OnUpgradeTier();
        }
    }
}